﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RulesEngine.Models;
using System.Linq;
using System.Collections.Generic;
using RulesEngine.Extensions;

namespace RulesEngine.API.Controllers
{
    [ApiController]
    [Route("workflow")]
    public class WorkflowController : ControllerBase
    {
        /// <summary>
        /// Implementación básica del ruleEngine
        /// </summary>
        /// <param name="workflowName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost("{workflowName}")]
        public async Task<dynamic> PostValidateWorkflow([FromRoute] string workflowName, [FromBody] dynamic parameters)
        {
            var workflowRules = await System.IO.File.ReadAllTextAsync(@"Resources/workflows.json", CancellationToken.None);
            var workflows = JsonConvert.DeserializeObject<Workflow[]>(workflowRules);
            var reSettingsWithCustomTypes = new ReSettings { CustomTypes = new Type[] { typeof(PropertyExtensions) } };
            var rulesEngine = new RulesEngine(workflows, null, reSettingsWithCustomTypes);


            var workflowRequested = workflows.FirstOrDefault(w => w.WorkflowName == workflowName);

            if (workflowRequested == null)
            {
                return $"No existe el workflow {workflowName}";
            }
            var ruleParameters = await GetRuleParameter(workflowName, parameters);
            List<RuleResultTree> result = await rulesEngine.ExecuteAllRulesAsync(workflowRequested.WorkflowName, ruleParameters.ToArray());

            Dictionary<string, bool> dictionary = result.ToDictionary(test => test.Rule.RuleName, test => test.IsSuccess);

            return dictionary;
        }

        /// <summary>
        /// Ejemplo con mensaje custom
        /// </summary>
        /// <param name="workflowName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost("ValidateWorkflowWithCustomMessage/{workflowName}")]
        public async Task<string> PostValidateWorkflowWithCustomMessage([FromRoute] string workflowName, [FromBody] dynamic parameters)
        {
            var workflowRules = await System.IO.File.ReadAllTextAsync(@"Resources/workflows.json", CancellationToken.None);
            var workflows = JsonConvert.DeserializeObject<Workflow[]>(workflowRules);

            ReSettings reSettings = new ReSettings {CustomTypes = new Type[] { typeof(PropertyExtensions) }};
            var rulesEngine = new RulesEngine(workflows, null,reSettings);


            var workflowRequested = workflows.FirstOrDefault(w => w.WorkflowName == workflowName);

            if (workflowRequested == null)
            {
                return $"No existe el workflow {workflowName}";
            }
            var ruleParameters = await GetRuleParameter(workflowName, parameters);
            List<RuleResultTree> result = await rulesEngine.ExecuteAllRulesAsync(workflowRequested.WorkflowName, ruleParameters.ToArray());
            
            string discountOffered = string.Empty;

            result.OnSuccess((eventName) =>
            {
                
                discountOffered = $"Descuento: {eventName}";
            });
            result.OnFail(() =>
            {
                discountOffered = $"Fallido";
            });
           
            return discountOffered;
        }

        /// <summary>
        /// Cerveza con stock disponible, implementación con local variables o rules compuestas.
        /// </summary>
        /// <param name="workflowName"></param>
        /// <param name="beerName"></param>
        /// <returns></returns>
        [HttpGet("SaleNationalBeer/{workflowName}/beer/{beerName}")]
        public async Task<string> PostSaleNationalBeer([FromRoute] string workflowName, [FromRoute] string beerName)
        {
            var workflowRules = await System.IO.File.ReadAllTextAsync(@"Resources/workflows.json", CancellationToken.None);
            var workflows = JsonConvert.DeserializeObject<Workflow[]>(workflowRules);

            ReSettings reSettings = new ReSettings { CustomTypes = new[] { typeof(PropertyExtensions) } };
            var rulesEngine = new RulesEngine(workflows, null, reSettings);


            if (workflows != null)
            {
                var workflowRequested = workflows.FirstOrDefault(w => w.WorkflowName == workflowName);

                if (workflowRequested == null)
                {
                    return $"No existe el workflow {workflowName}";
                }

                var ruleParameters = new List<RuleParameter>
                {
                    new RuleParameter("Beer",new
                    {
                        Name = beerName
                    } )
                };

                List<RuleResultTree> result = await rulesEngine.ExecuteAllRulesAsync(workflowRequested.WorkflowName, ruleParameters.ToArray());

                string discountOffered = string.Empty;

                result.OnSuccess((eventName) =>
                {

                    discountOffered = $"Producto Disponible";
                });
                result.OnFail(() =>
                {
                    discountOffered = $"No Disponible";
                });

                return discountOffered;
            }

            return $"No existe el workflow {workflowName}";
        }

        private async Task<List<RuleParameter>> GetRuleParameter(string workflowName, dynamic parameters)
        {
            var parameterDynamic = JsonConvert.DeserializeObject<dynamic>(parameters.ToString());
            var ruleParameters = new List<RuleParameter>();

            switch (workflowName)
            {
                case "DiscountWithCustomInputNames":

                    ruleParameters.Add(new RuleParameter("basicInfo", new
                    {
                        country = Convert.ToString(parameterDynamic.basicInfo.country),
                        loyalityFactor = (int)int.Parse(parameterDynamic.basicInfo.loyaltyFactor.ToString()),
                        totalPurchasesToDate = (int)int.Parse(parameterDynamic.basicInfo.totalPurchaseToDate.ToString()),
                    }));
                    ruleParameters.Add(new RuleParameter("orderInfo", new
                    {
                        totalOrders = (int)int.Parse(parameterDynamic.orderInfo.totalOrders.ToString())
                    }));
                    ruleParameters.Add(new RuleParameter("telemetryInfo", new
                    {
                        noOfVisitsPerMonth = (int)int.Parse(parameterDynamic.telemetryInfo.noOfVisitsPerMonth.ToString())
                    }));
                    break;
                default:
                {
                    throw new Exception("No existe el workflow");
                }
            }

            return await Task.FromResult(ruleParameters);
        }
    }

    public static class PropertyExtensions
    {
        public static object GetPropertyByName(this object parameter, string propertyName)
        {
            var value = parameter.GetType().GetProperty(propertyName)?.GetValue(parameter, null);
            return value;
        }

        public static string GetPropertyStringByName(this object parameter, string propertyName)
        {
            return parameter.GetPropertyByName(propertyName)?.ToString();
        }

        public static int GetPropertyIntByName(this object parameter, string propertyName)
        {
            if (int.TryParse(parameter.GetPropertyStringByName(propertyName), out int result))
            {
                return result;
            }

            return 0;
        }

        public static bool ComparerTotalOrders(int value)
        {
            return value.Equals(3);
        }

        public static bool BeerImported(string beer)
        {
            return beer.ToLower().Trim() switch
            {
                "polar" => true,
                "solera" => true,
                _ => false
            };
        }

        public static bool HaveStock(string beer)
        {
            return beer.ToLower().Trim() switch
            {
                "polar" => true,
                "solera" => true,
                _ => false
            };
        }


    }
}
